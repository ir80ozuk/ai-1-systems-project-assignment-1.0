# AI 1 systems project Assignment 1.0



## Getting started

This repo contains AI 1 system project Assignment 1.0 in a zip file (to execute the code please extract the files). 
The zip file contains all the .py files to execute the code.

## Files

### main.py
This .py file runs the program for all the files in the problems folder and saves it into folder named solutions

### check.py
This .py validate the instructions given inside the check problems abc.

### find.py
This .py finds a plan to cover all the empty spaces given inside cave in the find problems def.

### wumpus.py
This .py loads the txt files according to the problem and create an object instance for the particular file.


## Approach

### check
In the begining the function checks the starting position, if available it started following the given intructions from the starting point and saves the coordinates of the points it is covering following the path, finally checks the list of coordinates from the total empty spaces to check whether the robot move through all the empty spaces and if the starting position is not known the robot intialize through each of empty spaces and saves the coordinates of the points in the path for each iteration as a list of sets, after that intersection is calculated among the sets so that the points that are not covered will be separated, finally checks the list of coordinates from the total empty spaces to check whether the robot move through all the empty spaces

### find
For this time I use the stretegy for random initializations from the empty points to find the best possible (least cost) solution, it is a greedy approach but later I will work on BFS to do the task more efficiently.

## Problems
Most of the problem occured in the find task either for very high total cost or a missed point, but if I implement more sophisticated algo to solve this task this problem will be eliminated.

